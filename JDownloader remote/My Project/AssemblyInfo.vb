﻿Imports System
Imports System.Reflection
Imports System.Resources
Imports System.Runtime.InteropServices

' Allgemeine Informationen über eine Assembly werden über die folgende 
' Attributgruppe gesteuert. Ändern Sie diese Attributwerte, um die
' Assemblyinformationen zu ändern.

' Die Werte der Assemblyattribute überprüfen

<Assembly: AssemblyTitle("JDownloader remote")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Microsoft")> 
<Assembly: AssemblyProduct("JDownloader remote")> 
<Assembly: AssemblyCopyright("Copyright @ Microsoft 2011")> 
<Assembly: AssemblyTrademark("")>
<Assembly: ComVisible(False)>

'Um mit dem Erstellen lokalisierbarer Anwendungen zu beginnen, legen Sie 
'<UICulture>ImCodeVerwendeteKultur</UICulture> in der VBPROJ-Datei
'in einer <PropertyGroup> fest. Wenn Sie in den Quelldateien beispielsweise Deutsch 
'(Deutschland) verwenden, legen Sie <UICulture> auf de-DE fest. Heben Sie dann die Auskommentierung
'des nachstehenden NeutralResourceLanguage-Attributs auf. Aktualisieren Sie ""de-DE"" in der nachstehenden Zeile,
'sodass das Attribut mit der UICulture-Einstellung in der Projektdatei übereinstimmt.

<Assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)> 

'Das ThemeInfo-Attribut beschreibt, wo Sie themenspezifische und generische Ressourcenverzeichnisse finden.
'1. Parameter: Speicherort der themenspezifischen Ressourcenverzeichnisse
'(wird verwendet, wenn eine Ressource auf der Seite nicht gefunden werden kann, 
' oder im Anwendungsressourcenverzeichnis nicht gefunden werden kann)

'2. Parameter: Speicherort des generischen Ressourcenverzeichnisses
'(wird verwendet, wenn eine Ressource auf der Seite nicht gefunden werden kann, 
'in der Anwendung oder in anderen themenspezifischen Ressourcenverzeichnissen nicht gefunden werden kann)
'<Assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)>

'Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
<Assembly: Guid("2931a70b-ff52-482f-8ba9-2fbb2b7cad16")> 

' Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
'
'      Hauptversion
'      Nebenversion 
'      Buildnummer
'      Revision
'
' Sie können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
' übernehmen, indem Sie "*" eingeben:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")>